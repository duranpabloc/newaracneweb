export class Sponsor {
  constructor(
    public name: string,
    public logoUrl: string,
    public description: string,
    public siteUrl: string
  ) {}
}
