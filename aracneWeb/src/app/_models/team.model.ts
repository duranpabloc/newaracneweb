export class Team {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public foundation: number,
    public active: boolean,
    public imageUrl: string,
    public players: number[]
  ) {}
}
