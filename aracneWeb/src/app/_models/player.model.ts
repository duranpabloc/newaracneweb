export class Player {
  constructor(
    public id: number,
    public name: string,
    public role: string,
    public about: string,
    public active: boolean,
    public playerImg: string,
    public channels: any,
    public social: any
  ) {}
}
