import { Component, OnInit } from '@angular/core';

import aracneData from "../../_files/AracneData.json";
import { Team } from 'src/app/_models/team.model.js';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  teams: Team[] = aracneData.teams;

  constructor() { }

  ngOnInit() {
  }

}
