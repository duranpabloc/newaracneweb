import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Player } from '../../_models/player.model';
import { Team } from 'src/app/_models/team.model';
import aracneData from '../../_files/AracneData.json';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {
  teamId: number;
  team: Team;
  players: Player[];

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params.id !== undefined) {
        this.teamId = +params.id;
      } else {
        this.teamId = 1;
      }
      this.team = this.getTeam(this.teamId, aracneData.teams);
      this.players = this.getPlayers(this.team.players, aracneData.players);
    });
  }

  private getTeam(id: number, teams: Team[]) {
    for (const team of teams) {
      if (team.id === id) { return team; }
    }
  }

  private getPlayers(playerNumbers: number[], players: Player[]) {
    const playerFilter: Player[] = [];
    for (const player of players) {
      if (playerNumbers.includes(player.id)) { playerFilter.push(player); }
    }
    return playerFilter;
  }
}
