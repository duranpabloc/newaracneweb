import { Component, Pipe, PipeTransform, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Player } from 'src/app/_models/player.model.js';
import aracneData from '../../_files/AracneData.json';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit {
  player: Player;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params.id !== undefined) {
        this.player = this.getPlayer(+params.id, aracneData.players);
      } else {
        this.player = {
          id: 1,
          name: 'pepito',
          role: 'quien sabe',
          about: 'que se sho',
          active: true,
          playerImg: '',
          channels: [],
          social: []
        };
      }
    });
  }

  getPlayer(playerId: number, players:Player[]) : Player {
    for (let p of players) {
      if (p.id === playerId) { return p; }
    }
  }
}
