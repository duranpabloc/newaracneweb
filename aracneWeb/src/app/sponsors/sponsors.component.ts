import { Component, OnInit } from '@angular/core';

import { Sponsor } from '../_models/sponsor.model.js';

import aracneData from '../_files/aracneData.json';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.css']
})
export class SponsorsComponent implements OnInit {
  sponsors: Sponsor[];
  constructor() { }

  ngOnInit() {
    this.sponsors = aracneData.sponsors;
  }

}
