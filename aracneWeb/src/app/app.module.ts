import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent, SafePipe } from './app.component';
import { TeamsComponent } from './teams/teams.component';
import { HeaderComponent } from './header/header.component';
import { TeamListComponent } from './teams/team-list/team-list.component';
import { TeamDetailComponent } from './teams/team-detail/team-detail.component';
import { TeamItemComponent } from './teams/team-list/team-item/team-item.component';
import { PlayersComponent } from './players/players.component';
import { PlayerListComponent } from './players/player-list/player-list.component';
import { PlayerItemComponent } from './players/player-list/player-item/player-item.component';
import { PlayerDetailComponent } from './players/player-detail/player-detail.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { SponsorItemComponent } from './sponsors/sponsor-item/sponsor-item.component';
import { ApplyComponent } from './apply/apply.component';
import { UnderconstructionComponent } from './underconstruction/underconstruction.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamsComponent,
    HeaderComponent,
    TeamListComponent,
    TeamDetailComponent,
    TeamItemComponent,
    PlayersComponent,
    PlayerListComponent,
    PlayerItemComponent,
    PlayerDetailComponent,
    FooterComponent,
    HomeComponent,
    SponsorsComponent,
    PagenotfoundComponent,
    SponsorItemComponent,
    SafePipe,
    ApplyComponent,
    UnderconstructionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
