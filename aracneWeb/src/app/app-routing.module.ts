import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TeamsComponent } from "./teams/teams.component";
import { PlayersComponent } from "./players/players.component";
import { PlayerListComponent } from "./players/player-list/player-list.component";
import { PlayerDetailComponent } from "./players/player-detail/player-detail.component";
import { TeamListComponent } from "./teams/team-list/team-list.component";
import { HomeComponent } from "./home/home.component";
import { SponsorsComponent } from './sponsors/sponsors.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ApplyComponent } from './apply/apply.component';
import { UnderconstructionComponent } from './underconstruction/underconstruction.component';

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  {
    path: "teams",
    component: TeamsComponent,
    children: [
      { path: "", component: TeamListComponent, pathMatch: "full" },
      { path: ":id", component: PlayerListComponent }
    ]
  },
  {
    path: "player",
    component: PlayersComponent,
    children: [{ path: ":id", component: PlayerDetailComponent }]
  },
  {
    path: "sponsors",
    component: SponsorsComponent,
    data: {title: "Patrocinadores"}
  },
  {
    path: "apply",
    component: ApplyComponent,
    data: {title: "Únete"}
  },
  {
    path: "underconstruction",
    component: UnderconstructionComponent,
    data: {title: "En construcción"}
  },
  {
    path: "**",
    component: PagenotfoundComponent,
    data: {title: "Error 404"}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
